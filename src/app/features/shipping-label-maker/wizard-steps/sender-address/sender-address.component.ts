import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppDataService } from 'src/app/services/app-data.service';

@Component({
  selector: 'app-sender-address',
  templateUrl: './sender-address.component.html',
  styleUrls: ['./sender-address.component.scss']
})
export class SenderAddressComponent implements OnInit {

  senderForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private appDataService: AppDataService) { 
    this.senderForm = new FormGroup({});
  }

  ngOnInit(): void {

    const { from } = this.appDataService.shippingInfo;
    this.senderForm = this.formBuilder.group({
      name: [from.name, Validators.required],
      street: [from.street],
      city: [from.city],
      state: [from.state],
      zip: [from.zip, [
        Validators.required,
        Validators.pattern("^[0-9]*$"),
        Validators.minLength(5),
      ]]
    });
    
    this.senderForm.valueChanges.subscribe(
      (value) => {
        if(this.senderForm && this.senderForm.invalid) {
          this.appDataService.updateStepFromStatus({ step1FormValid: false});
        } else {
          console.log("Valid Form 1");
          this.appDataService.updateStepFromStatus({ step1FormValid: true});
        }
        this.appDataService.updateFormState({'from': value});
      }
    )
  }

  get senderFormControl() {
    return this.senderForm.controls;
  }

  onSubmit(formData: FormGroup){
    
  }

  fillSenderAddress(){
    this.senderForm.setValue({
      name: 'Sender Address',
      street: 'Sender Street address',
      city: 'Sender City',
      state: 'sender State',
      zip: '12345456'
    })
    this.senderForm.updateValueAndValidity({ emitEvent: true, onlySelf: false });
  }

}
