import { Component, OnInit } from '@angular/core';
import { ShippingInfo } from 'src/app/models/shipping-info.model';
import { ShippingOption, SHIPPING_RATE } from 'src/app/models/utils.constant';
import { AppDataService } from 'src/app/services/app-data.service';

@Component({
  selector: 'app-confirm-screen',
  templateUrl: './confirm-screen.component.html',
  styleUrls: ['./confirm-screen.component.scss']
})
export class ConfirmScreenComponent implements OnInit {

  shippingInfo: ShippingInfo;
  displayShippingOption: string = "";
  shippingCost: number = 0;

  confirmMsg: string = "Are you sure?";

  constructor(private appDataService: AppDataService){
    this.shippingInfo = this.appDataService.shippingInfo;
  }

  ngOnInit(): void {
    this.shippingInfo = this.appDataService.shippingInfo;
    this.displayShippingOption = this.shippingInfo.shippingOption;

    this.shippingCost = (this.shippingInfo.weight || 0 ) * SHIPPING_RATE * (this.displayShippingOption === ShippingOption[ShippingOption.Ground] ? 1 : 1.5);
  }

}
