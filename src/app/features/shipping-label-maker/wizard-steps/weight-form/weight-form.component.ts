import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppDataService } from 'src/app/services/app-data.service';

@Component({
  selector: 'app-weight-form',
  templateUrl: './weight-form.component.html',
  styleUrls: ['./weight-form.component.scss']
})
export class WeightFormComponent implements OnInit {

  weightForm: FormGroup;
  constructor(private appDataService: AppDataService, private formBuilder: FormBuilder) {
    this.weightForm = new FormGroup({});
  }

  ngOnInit() {
    const { weight } = this.appDataService.shippingInfo;
    this.weightForm = this.formBuilder.group({
      weight: [weight, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(2)]]
    });

    this.weightForm.valueChanges.subscribe((value) => {
      if (this.weightForm.invalid) {
        this.appDataService.updateStepFromStatus({ step3FormValid: false});
      } else {
        this.appDataService.updateStepFromStatus({ step3FormValid: true});
      }
      this.appDataService.updateFormState({ 'weight': value.weight });
    })
  }


  get weightFormControl() {
    return this.weightForm.controls;
  }

  onSubmit(formData: FormGroup) {

  }

}
