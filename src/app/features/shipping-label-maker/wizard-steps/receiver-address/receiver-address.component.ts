import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppDataService } from 'src/app/services/app-data.service';

@Component({
  selector: 'app-receiver-address',
  templateUrl: './receiver-address.component.html',
  styleUrls: ['./receiver-address.component.scss']
})
export class ReceiverAddressComponent implements OnInit {

  receiverForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private appDataService: AppDataService) {
    this.receiverForm = new FormGroup({});
  }

  ngOnInit(): void {

    const { to } = this.appDataService.shippingInfo;
    this.receiverForm = this.formBuilder.group({
      name: [to.name, Validators.required],
      street: [to.street, Validators.required],
      city: [to.city, Validators.required],
      state: [to.state, Validators.required],
      zip: [to.zip, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(5), Validators.maxLength(6)]]
    });

    this.receiverForm.valueChanges.subscribe(
      (formData) => {
        if (this.receiverForm && this.receiverForm.invalid) {
          this.appDataService.updateStepFromStatus({ step2FormValid: false});
        } else {
          this.appDataService.updateStepFromStatus({ step2FormValid: true});
        }
        this.appDataService.updateFormState({'to': formData});
      }
    )
  }

  get receiverFormControl() {
    return this.receiverForm.controls;
  }

  onSubmit(formData: FormGroup) {

  }


  fillReceiverAddress(){
    this.receiverForm.setValue({
      name: 'Receiver Address',
      street: 'Receiver Street address',
      city: 'Receiver City',
      state: 'Receiver State',
      zip: '999999'
    })
    this.receiverForm.updateValueAndValidity({ emitEvent: true, onlySelf: false });
  }

}
