import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ShippingOption } from 'src/app/models/utils.constant';
import { AppDataService } from 'src/app/services/app-data.service';

@Component({
  selector: 'app-shipping-option',
  templateUrl: './shipping-option.component.html',
  styleUrls: ['./shipping-option.component.scss']
})
export class ShippingOptionComponent implements OnInit {

  shippingOptionForm: FormGroup;
  shippingOption = ShippingOption;
  keys: string[];

  constructor(private appDataService: AppDataService, private formBuilder: FormBuilder) {
    this.shippingOptionForm = this.formBuilder.group({});
    this.keys = Object.keys(this.shippingOption).filter(f => isNaN(Number(f)));
  }

  ngOnInit() {
    const { shippingOption } = this.appDataService.shippingInfo;
    this.shippingOptionForm = this.formBuilder.group({
      shippingOption: [shippingOption, [
        Validators.required
      ]]
    });
    this.shippingOptionForm.valueChanges.subscribe(
      (value) => {
        if (this.shippingOptionForm.invalid) {
          this.appDataService.updateStepFromStatus({ step4FormValid: false});
        } else {
          this.appDataService.updateStepFromStatus({ step4FormValid: true});
        }
        this.appDataService.updateFormState({'shippingOptionLabel': this.shippingOption[value.shippingOption]});
        this.appDataService.updateFormState({'shippingOption': value.shippingOption});
      }
    )
  }

  onSubmit(formData: FormGroup){}

}
