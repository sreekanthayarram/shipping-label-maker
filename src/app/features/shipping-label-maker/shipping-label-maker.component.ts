import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShippingInfo } from 'src/app/models/shipping-info.model';
import { AppDataService } from 'src/app/services/app-data.service';

@Component({
  selector: 'app-shipping-label-maker',
  templateUrl: './shipping-label-maker.component.html',
  styleUrls: ['./shipping-label-maker.component.scss']
})
export class ShippingLabelMakerComponent implements OnInit {

  shippingInfo: ShippingInfo;
  progressValue: number = 0;

  constructor(private router: Router, private appDataService: AppDataService) {
    this.shippingInfo = {
      from: {
        name: '',
        street: '',
        city: '',
        state: '',
        zip: ''
      },
      to: {
        name: '',
        street: '',
        city: '',
        state: '',
        zip: ''
      },
      weight: null,
      shippingOption: ''
    }
  }

  ngOnInit(): void {
    this.progressValue = this.appDataService.currentStep * 20;
    this.appDataService.stepChange$.subscribe((currentStep: number) => {
      this.progressValue = currentStep * 20;
    });
  }

  generateShippingLabel() {
    this.progressValue = 100;
    this.appDataService.updateStep(5);
    this.router.navigate(['/label']);
  }

}
