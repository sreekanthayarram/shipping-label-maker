import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MailingAddress, ShippingInfo } from '../models/shipping-info.model';
import { routeMapper, stepMapper } from '../models/utils.constant';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {

  currentStep: number = 0;

  formValidStatus: any = {
    step1FormValid: false,
    step2FormValid: false,
    step3FormValid: false,
    step4FormValid: false
  }
  shippingInfo: ShippingInfo = {
    from: {
      name: '',
      street: '',
      city: '',
      state: '',
      zip: ''
    },
    to: {
      name: '',
      street: '',
      city: '',
      state: '',
      zip: ''
    },
    weight: null,
    shippingOption: ''
  }

  private stepSubject = new BehaviorSubject(0);
  stepChange$ = this.stepSubject.asObservable();

  private confirmSubject = new BehaviorSubject(undefined);
  confirmStep$ = this.confirmSubject.asObservable();

  private stepFormStatusSubject = new BehaviorSubject(this.formValidStatus);
  stepFormStatus$ = this.stepFormStatusSubject.asObservable();

  constructor() { }

  pathForCurrentStep = () => {
    return routeMapper(this.currentStep);
  }
  changeStep = (action: number) => {
    switch (action) {
      case 1:
        this.currentStep >= 1 ? this.currentStep-- : this.currentStep = 0;
        break;
      case 2:
        this.currentStep <= 4 ? this.currentStep++ : this.currentStep = 0;
        break;
      case 3:
        this.currentStep = 5;
        break;
      default: break;
    }
    this.stepSubject.next(this.currentStep);
    return this.pathForCurrentStep();
  }

  updateFormState(newUpdatedObj: any) {
    this.shippingInfo = <any>{ ...this.shippingInfo, ...newUpdatedObj };
  }

  updateStepFromStatus(formStatus: any){
    this.formValidStatus = <any>{ ...this.formValidStatus, ...formStatus };
    this.stepFormStatusSubject.next(this.formValidStatus);
  }

  setCurrentStep(val: string) {
    this.currentStep = stepMapper(val);
    this.stepSubject.next(this.currentStep);
  }

  updateStep(stepNumber: number) {
    this.currentStep = stepNumber;
    this.stepSubject.next(this.currentStep);
  }
}
