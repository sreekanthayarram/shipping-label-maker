import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardComponent } from './wizard/wizard.component';
import { StepHeaderComponent } from './step-header/step-header.component';



@NgModule({
  declarations: [
    WizardComponent,
    StepHeaderComponent
  ],
  exports: [
    WizardComponent,
    StepHeaderComponent
  ],
  
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
