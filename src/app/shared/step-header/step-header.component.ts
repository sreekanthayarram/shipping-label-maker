import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { stepHeaderMapper } from 'src/app/models/utils.constant';
import { AppDataService } from 'src/app/services/app-data.service';

@Component({
  selector: 'app-step-header',
  templateUrl: './step-header.component.html',
  styleUrls: ['./step-header.component.scss']
})
export class StepHeaderComponent implements OnInit {

  stepName: string = '';

  constructor(private appDataService: AppDataService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.stepName = stepHeaderMapper(this.appDataService.currentStep);
    // this.appDataService.setCurrentStep(this.activatedRoute.snapshot.url[0].path || this.router.url);
      this.appDataService.stepChange$.subscribe((currentStep: number) => {
          this.stepName = stepHeaderMapper(currentStep);
      });
  }

}
