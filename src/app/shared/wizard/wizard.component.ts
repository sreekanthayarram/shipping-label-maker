import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { ShippingInfo } from 'src/app/models/shipping-info.model';
import { WizardAction } from 'src/app/models/utils.constant';
import { AppDataService } from 'src/app/services/app-data.service';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements OnInit {

  @Input() wizardContext: ShippingInfo | undefined;
  @Output() complete = new EventEmitter<{ status: boolean }>();

  showCompleteBtn: boolean = false;
  showPrevBtn: boolean = false;
  isFormValid: boolean = false;
  isCompleted: boolean = false;
  disableNextBtn: boolean = false;

  constructor(private appDataService: AppDataService, private router: Router) { }

  ngOnInit(): void {
    // this.appDataService.stepChange$.subscribe((currentStep: number) => {
    //   console.log('WizardComponent inside step chage', );
    //   this.showCompleteBtn = currentStep === 4;
    //   this.isCompleted = currentStep === 5;
    //   this.showPrevBtn = currentStep !== 0;
    //   this.isformValid = Object.keys(this.appDataService.formValidStatus).some(key => !this.appDataService.formValidStatus[key]);
    // });


    combineLatest([this.appDataService.stepChange$, this.appDataService.stepFormStatus$]).subscribe(
      (data) => {
        this.isFormValid = Object.keys(this.appDataService.formValidStatus).some(key => this.appDataService.formValidStatus[key]);
        const currentStep = data[0];
        const updatedFormsStatus = data[1];
        
        this.showCompleteBtn = currentStep === 4;
        this.isCompleted = currentStep === 5;
        this.showPrevBtn = currentStep !== 0;
        if (currentStep == 0) {
          this.disableNextBtn = updatedFormsStatus.step1FormValid ? false : true;
        } else if (currentStep == 1) {
          this.disableNextBtn = updatedFormsStatus.step2FormValid ? false : true;
        } else if (currentStep == 2) {
          this.disableNextBtn = updatedFormsStatus.step3FormValid ? false : true;
        } else if (currentStep == 3) {
          this.disableNextBtn = updatedFormsStatus.step4FormValid ? false : true;
        } 
      }
    );
  }

  gotoPrevStep() {
    this.router.navigate([this.appDataService.changeStep(WizardAction.Prev)]);
  }

  gotoNextStep() {
    this.router.navigate([this.appDataService.changeStep(WizardAction.Next)]);
  }

  generateShippingLabel() {
    this.complete.emit({ status: true });
  }

  goToFirst(){
    if(this.isCompleted){
      window.location.reload();      
    }
  }

}
