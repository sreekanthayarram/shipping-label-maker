import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { ShippingLabelMakerComponent } from './features/shipping-label-maker/shipping-label-maker.component';
import { WizardStepsComponent } from './features/shipping-label-maker/wizard-steps/wizard-steps.component';
import { SharedModule } from './shared/shared.module';
import { SenderAddressComponent } from './features/shipping-label-maker/wizard-steps/sender-address/sender-address.component';
import { ReceiverAddressComponent } from './features/shipping-label-maker/wizard-steps/receiver-address/receiver-address.component';
import { WeightFormComponent } from './features/shipping-label-maker/wizard-steps/weight-form/weight-form.component';
import { ShippingOptionComponent } from './features/shipping-label-maker/wizard-steps/shipping-option/shipping-option.component';
import { ConfirmScreenComponent } from './features/shipping-label-maker/wizard-steps/confirm-screen/confirm-screen.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LabelComponent } from './features/shipping-label-maker/wizard-steps/label/label.component';

@NgModule({
  declarations: [
    AppComponent,
    ShippingLabelMakerComponent,
    WizardStepsComponent,
    SenderAddressComponent,
    ReceiverAddressComponent,
    WeightFormComponent,
    ShippingOptionComponent,
    ConfirmScreenComponent,
    LabelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    ReactiveFormsModule,

    MatToolbarModule,
    MatProgressBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
