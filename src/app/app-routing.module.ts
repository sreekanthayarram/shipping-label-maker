import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmScreenComponent } from './features/shipping-label-maker/wizard-steps/confirm-screen/confirm-screen.component';
import { LabelComponent } from './features/shipping-label-maker/wizard-steps/label/label.component';
import { ReceiverAddressComponent } from './features/shipping-label-maker/wizard-steps/receiver-address/receiver-address.component';
import { SenderAddressComponent } from './features/shipping-label-maker/wizard-steps/sender-address/sender-address.component';
import { ShippingOptionComponent } from './features/shipping-label-maker/wizard-steps/shipping-option/shipping-option.component';
import { WeightFormComponent } from './features/shipping-label-maker/wizard-steps/weight-form/weight-form.component';

const routes: Routes = [
  { path: '', redirectTo: '/sender-step', pathMatch: 'full' },
  { path: 'sender-step', component: SenderAddressComponent },
  { path: 'receiver-step', component: ReceiverAddressComponent },
  { path: 'weight-step', component: WeightFormComponent },
  { path: 'ship-option-step', component: ShippingOptionComponent },
  { path: 'confirm', component: ConfirmScreenComponent },
  { path: 'label', component: LabelComponent },
  { path: '**', component: SenderAddressComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
