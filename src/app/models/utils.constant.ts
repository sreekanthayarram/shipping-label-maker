
export const routeMapper = (step: number) => {
    switch (step) {
        case 0: return "sender-step";
        case 1: return "receiver-step";
        case 2: return "weight-step";
        case 3: return "ship-option-step";
        case 4: return "confirm";
        case 5: return "label";
        default: return "sender-step";
    }
}

export const stepMapper = (path: string) => {
    switch (path) {
        case "/sender-step": return 0;
        case "/receiver-step": return 1;
        case "/weight-step": return 2;
        case "/ship-option-step": return 3;
        case "/confirm": return 4;
        case "/label": return 5;
        default: return 0;
    }
}

export const stepHeaderMapper = (step: number) => {
    switch (step) {
        case 0: return "Enter sender's address";
        case 1: return "Enter receiver's address";
        case 2: return "Enter weight";
        case 3: return "Enter shipping option";
        case 4: return "Confirm";
        case 5: return "Your label";
        default: return "Enter sender's address";
    }
}

export const WizardAction = {
    Prev: 1,
    Next: 2,
    End: 3
};

export const SHIPPING_RATE = 0.40;

export enum ShippingOption {
  Ground = 1,
  Priority = 2
}