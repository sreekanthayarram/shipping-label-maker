export interface MailingAddress {
    name: string,
    street: string,
    city: string,
    state: string,
    zip: string
}

export interface ShippingInfo {
    from: MailingAddress,
    to: MailingAddress,
    weight: number|null,
    shippingOption: string
}